async function fetchData() {
  try {
    const response = await fetch('https://jsonserver.maakeetoo.com/search-app?title_like=');
    if (!response.ok) {
      throw new Error('Request failed');
    }
    const data = await response.json();
    return data;
  } catch (error) {
    console.error('Error:', error.message);
  }
}


export default fetchData;
