import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';

function Header() {
  return (
    <>
      <Navbar style={{backgroundColor:'white'}}>
     
          <Navbar.Brand href="#home">
            <img
              src="media/logo.svg"
              width="125"
              height="25"
              className="d-inline-block align-top"
              alt="React Bootstrap logo"
            />
          </Navbar.Brand>
       
      </Navbar>
      <br />
      
    </>
  );
}

export default Header;