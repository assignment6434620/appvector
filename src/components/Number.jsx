import React from "react";

function Number(props){
    return(
        <div style={{width:'fit-content',marginRight:'auto',marginLeft:'auto',backgroundColor:'#ddf8fc',padding:'0 1rem'}}>
            <h3 style={{textAlign:"center",color:'#4fc9da'}}>{props.number}</h3>
            
        </div>
    )
}

export default Number;
