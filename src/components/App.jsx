import React from "react";
import Header from "./Header";
import Main from "./Main-section";
import { Container } from "react-bootstrap";
import YourApp from "./your-app/YourApp";
function App() {
    return (
        <>
            <Header />
            <div style={{margin:'0 2rem'}}>
                <Main />

                <YourApp />
            </div>

        </>
    )
}

export default App;