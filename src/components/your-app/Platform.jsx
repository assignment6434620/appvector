import React from "react";
import { Col, Container, Form, Row } from "react-bootstrap";


async function fetchApps() {

}


function Platform() {
    return (
        <Container>
            <Row>
                <h3>Select Platform</h3>
                <Col>

                    <div className="form-check form-check-custom form-check-solid">
                        <input className="form-check-input" type="radio" value="" id="flexRadioDefault" />
                        <label className="form-check-label" htmlFor="flexRadioDefault">
                            Android  </label>
                    </div>

                    <div className="form-check form-check-custom form-check-solid">
                        <input className="form-check-input" type="radio" value="" disabled id="flexRadioDisabled" />
                        <label className="form-check-label" htmlFor="flexRadioDisabled">
                           IOS
                        </label>
                    </div>

                </Col>
                <Col>
                    <div className="mt-2">
                        <i className="fa-brands fa-google-play fa-lg"></i>
                    </div>
                </Col>
            </Row>
        </Container>

    )
}

export default Platform;