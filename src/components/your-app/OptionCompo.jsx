import React, { useState } from "react";


function OptionCompo(props) {
    const [isMouseOver, setMouseOver] = useState(false);
    const [isMouseLeave, setMouseLeave] = useState(false);


    function handleMouseOver() {
        setMouseOver(true);
        setMouseLeave(false);
    }

    function handleMouseLeave() {
        setMouseLeave(true);
        setMouseOver(false);
    }
    return (
        <div  className={(isMouseOver && !isMouseLeave) ? 'overlay' : ''}
            onMouseOver={handleMouseOver} onMouseLeave={handleMouseLeave}
        >
            <option className='mt-2 mx-1' style={{ fontSize: '1.2rem' }} >{props.country}</option>

        </div>
    )
}export default OptionCompo;