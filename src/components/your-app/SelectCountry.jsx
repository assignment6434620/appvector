import React, { useEffect, useState } from "react";
import { Container, Row } from "react-bootstrap";
import { v4 as uuidv4 } from 'uuid';
import OptionCompo from "./OptionCompo";


function SelectCountry() {
    const [countryData, setCountryData] = useState([]);
    const [matchData, setMatchData] = useState([])
    const [country, setCountry] = useState('');
    const [isDeleting, setDeleting] = useState(false);
    const [isEmpty, setEmpty] = useState(true);
    const [isDropped, setDropped] = useState(false);




    useEffect(() => {
        async function fetchData() {
            try {
                const response = await fetch('https://jsonserver.maakeetoo.com/get-mapping');
                if (!response.ok) {
                    throw new Error('Request failed');
                }
                const jsonData = await response.json();
                setCountryData(jsonData.mapping);
            } catch (error) {
                console.error('Error:', error.message);
            }
        }

        fetchData();
    }, []);


    function handleChange(e) {
        setCountry(e.target.value);
        findText(e.target.value);
        if (e.target.value !== '')
            setEmpty(false);
        else
            setEmpty(true);

    }

    function handleKeyDown(e) {
        if (e.key === "Backspace") {
            setDeleting(true);
            findText(e.target.value);
        }
    }

    function handleClick() {
        setDropped(true);
    }

    function findText(str) {



        if (isDeleting) {
            setDeleting(false);
            const found = countryData.filter((item) => {

                // Replace `propertyName` with the actual property name you want to search for in your JSON object.
                const country = item.country_name.toLowerCase();
                if (country.toLowerCase().startsWith(str.toLowerCase())) {
                    return item;
                }
            });
            setMatchData(found);
        } else {
            const found = countryData.filter((item) => {

                // Replace `propertyName` with the actual property name you want to search for in your JSON object.
                const country = item.country_name.toLowerCase();
                if (country.toLowerCase().startsWith(str.toLowerCase())) {
                    return item;
                }
            });
            setMatchData(found);
        }

    }

    return (
        <>
            <Container>
                <h3>
                    Select Countries
                </h3>
                <div className="w-lg-100 position-relative">
                    <input type="text" className="form-control form-control-solid"
                        placeholder="United States (default)"
                        name="country"
                        onChange={handleChange}
                        onKeyDown={handleKeyDown}
                        onClick={handleClick}


                    />

                    <div className="position-absolute translate-middle-y top-50 end-0 me-3">
                        <span className="svg-icon svg-icon-2hx">
                            <i className="fa-solid fa-caret-down"></i>
                        </span>
                    </div>
                </div>

                <div className="card mt-2" style={{ height: '250px', overflowY: 'auto', display: isEmpty && isDropped ? 'block' : 'none' }}>
                    <option className="mt-2" disabled >Top Countries</option>
                    <option style={{ fontSize: '1.2rem' }} className="mt-2 mx-1">United States</option>
                    <option style={{ fontSize: '1.2rem' }} className="mt-2 mx-1">India</option>
                    <option className="mt-2" disabled >Other countries</option>


                    {
                        countryData.map((country, i) => {
                            let id=uuidv4();
                            return (
                                <OptionCompo key={id} country={country.country_name} />
                                
                            )
                        })
                    }
                </div>
                <div className="card mt-2" style={{ height: '250px', overflowY: 'auto', display: isEmpty ? 'none' : 'block' }}>

                    {
                        matchData.map((country, i) => {
                            let id=uuidv4();
                            return (
                                <OptionCompo key={id} country={country.country_name} />

                            )
                        })
                    }
                </div>
            </Container>
        </>
    )
}

export default SelectCountry;