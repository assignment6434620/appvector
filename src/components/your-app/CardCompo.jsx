import React, { useState } from "react";
import { Col, Row } from "react-bootstrap";

function CardCompo(props) {

    const [isMouseOver,setMouseOver]=useState(false);
    const [isMouseLeave,setMouseLeave]=useState(false);
   

    function handleMouseOver(){
        setMouseOver(true);
        setMouseLeave(false);
    }

    function handleMouseLeave(){
        setMouseLeave(true);
        setMouseOver(false);
    }

    function handleClick(){
       props.selectedApp(props);
    }

    return (
        <Row className={(isMouseOver && !isMouseLeave) ? 'overlay':''}
         onMouseOver={handleMouseOver} 
         onMouseLeave={handleMouseLeave}
         onClick={handleClick}
         > 

            <Col  lg="4">
                <img src={props.src} alt={props.title} style={{ display: 'block', width: '50%', height: '50%', marginTop:'25%',marginLeft:'auto',marginRight:'auto'}} />
            </Col>
            <Col className="pt-3" lg="8">
                <h6>{props.title}  </h6>
                <Row>
                    <Col lg="7">
                    <p>{props.developer} </p>
                    </Col>
                    <Col lg="5">
                    <span>{props.rating} ⭐</span>

                    </Col>
                </Row>

            </Col>


        </Row>


    )
}

export default CardCompo;