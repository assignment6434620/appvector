import React, { useEffect, useState } from "react";
import { Col, Container, Form, Row } from "react-bootstrap";
import CardCompo from "./CardCompo";



function SearchApp() {
    const [data, setData] = useState([]);
    const [searchInput, setSearchInput] = useState('');
    const [isPasted, setPasted] = useState(false);
    const [matchedData, setMatchedData] = useState([]);
    const [isTyping,setTyping]=useState(false);

    const [selectedApp,setSelectedApp]=useState([]);
    const [isAppSelected,setAppSelected]=useState(false);

    let typingTimer;
    const typingDelay = 500; // Adjust the delay according to your needs


    function findText(str) {
        let pastedWord = str;

        if (str === '') {
            setMatchedData([]);
            setTyping(false);
        } else {
            if (isPasted) {
                setPasted(false);

                const found = data.filter((item) => {
                    // Replace `propertyName` with the actual property name you want to search for in your JSON object.
                    const title = item.title.toLowerCase();
                    if (title.includes(pastedWord.toLowerCase())) {
                        return item;
                    }
                });

                setMatchedData(found);
            } else {

                const found = data.filter((item) => {
                    // Replace `propertyName` with the actual property name you want to search for in your JSON object.
                    const title = item.title.toLowerCase();
                    if (title.includes(str.toLowerCase())) {
                        return item;
                    }
                });
                setMatchedData(found);
            }

        }


    }
    useEffect(() => {
        async function fetchData() {
            try {
                const response = await fetch('https://jsonserver.maakeetoo.com/search-app?title_like=');
                if (!response.ok) {
                    throw new Error('Request failed');
                }
                const jsonData = await response.json();
                setData(jsonData);
            } catch (error) {
                console.error('Error:', error.message);
            }
        }

        fetchData();
    }, []);

    function handleSearch(e) {
        setSearchInput(e.target.value);
        clearTimeout(typingTimer);
        typingTimer = setTimeout(() => {
            findText(e.target.value);
        }, typingDelay);
        setTyping(true);

    }
    function handlePaste(e) {

        setPasted(true);
        setTyping(true);

    }

    function callBack(app){
        setAppSelected(true);
       setSelectedApp(app);
    }

    function handleClose(){
        setAppSelected(false);
        setTyping(true);
    }

    return (
        <>



            <Container style={{display:isAppSelected ? 'block':'none'}}>
                <Row >
                    <Col lg="4">
                        <img src={selectedApp.src} alt={selectedApp.title} style={{ display: 'block', width: '50%', height: '50%', marginTop: '25%', marginLeft: 'auto', marginRight: 'auto' }} />
                    </Col>
                    <Col className="pt-3" lg="7">
                        <h6>{selectedApp.title}  </h6>
                        <Row>
                            <Col lg="7">
                                <p>{selectedApp.developer} </p>
                            </Col>
                            <Col lg="5">
                                <span>{selectedApp.rating} ⭐</span>

                            </Col>
                        </Row>

                    </Col>
                    <Col lg="1">
                    <i onClick={handleClose} className="fa-solid fa-xmark fa-xl" style={{color: '#d92708'}}></i>
                    </Col>


                </Row>

            </Container>


            <Container style={{display:isAppSelected ? 'none':'block'}}>
                <div>
                    <h3>Search your App</h3>
                    <Row className="mt-3">
                        <Col lg="12">

                            <div className="w-lg-100 position-relative">
                                <input type="text" className="form-control form-control-solid"
                                    placeholder="Search apps"
                                    name="search"
                                    onChange={handleSearch}
                                    onPaste={handlePaste}
                                />

                                <div className="position-absolute translate-middle-y top-50 end-0 me-3">
                                    <span className="svg-icon svg-icon-2hx">
                                        <i className="fa-solid fa-magnifying-glass fa-xl mt-3 "></i>
                                    </span>
                                </div>
                            </div>
                        </Col>
                        <Col className="mt-2" lg="12" style={{display:isTyping ? 'block':'none'}}>

                            <div className="card shadow-sm" style={{ height: '300px', overflowY: 'auto' }}>

                                {matchedData.map((item, i) => {
                                    return (
                                        <CardCompo key={i}
                                            src={item.app_icon}
                                            title={item.title}
                                            rating={item.rating}
                                            developer={item.developer_name}
                                            selectedApp={callBack}
                                        />
                                    )
                                })}


                            </div>

                        </Col>
                    </Row>
                </div>
            </Container>
        </>
    )
}

export default SearchApp;