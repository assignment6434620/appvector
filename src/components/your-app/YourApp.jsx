import React from "react";
import { Col, Container, Row } from "react-bootstrap";
import Platform from "./Platform";
import SearchApp from "./SearchApp";
import SelectCountry from "./SelectCountry";

function YourApp() {
    return (
        <section id="your-app">
            <Container>
                <Row>
                    <Col lg="3">
                        <Platform />
                    </Col>
                    <Col lg="3">
                        <SearchApp />
                    </Col>
                    <Col lg="3">
                        <SelectCountry/>
                    </Col>
                    <Col lg="3">

                    </Col>
                </Row>
            </Container>



        </section>

    )
}

export default YourApp;