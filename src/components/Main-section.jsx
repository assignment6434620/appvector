import React from "react";
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { Container } from "react-bootstrap";
import Number from "./Number";
function Main() {
    return (
        <section id="main-section">
            <Container>
                <Row>
                    <Col>
                        <Number number="1" />
                        <p className="main-info">Add your App</p>
                    </Col>
                    <Col>
                        <Number number="2" />
                        <p className="main-info">Add competitor App</p>
                    </Col>
                    <Col>
                    <Number number="3" />
                        <p className="main-info">Choose keywords</p>
                    </Col>
                </Row>
            </Container>



        </section>
    )
}

export default Main;